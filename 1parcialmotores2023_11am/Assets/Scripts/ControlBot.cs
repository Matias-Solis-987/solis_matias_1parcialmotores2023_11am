using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBot : MonoBehaviour
{
    public int hp = 100;
    public int rapidez = 5;
    public Transform jugador;
    public int daņoJugador = 10;
    private bool InfligirDaņo = false;
    public GameObject explosionPrefab;

    void Start()
    {
        jugador = GameObject.FindGameObjectWithTag("jugador").transform;
    }

    private void Update()
    {
        transform.LookAt(jugador);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }

    public void recibirDaņo(int daņo)
    {
        hp = hp - daņo;
        Debug.Log("HP actual: " + hp);

        if (hp <= 0)
        {
            Debug.Log("Bot destruido");
            ExplosionEnemigo script = GetComponent<ExplosionEnemigo>();
            if (script != null)
            {
                script.Explosion();
                GestorDeAudio.instancia.ReproducirSonido("Muerte");
            }
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("colisiona con " + collision.gameObject.tag);
        switch (collision.gameObject.tag)
        {
            case "bala":
                recibirDaņo(10);
                break;

            case "balaSniper":
                recibirDaņo(100);
                break;

            case "balaEscopeta":
                recibirDaņo(150);
                break;

            case "Granada":
                recibirDaņo(25);
                break;

            case "cohete":
                recibirDaņo(100);
                break;

            default:
                break;
        }
    }

    private void desaparecer()
    {
        Explotar();
        Destroy(gameObject);
    }

    private void Explotar()
    {
        if (explosionPrefab != null)
        {
            GameObject particulas = Instantiate(explosionPrefab, transform.position, Quaternion.identity) as GameObject;
            Destroy(particulas, 2);
        }
    }
}
