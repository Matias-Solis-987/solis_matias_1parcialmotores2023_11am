using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cohete : MonoBehaviour
{
    public float retardo = 3f;
    public float radioExplosion = 10f;
    public GameObject explosionCohetePrefab;
    private bool impactado = false;
    public bool yaExplotado = false;

    void OnCollisionEnter(Collision collision)
    {
        if (!impactado)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("Pared") ||
                collision.gameObject.layer == LayerMask.NameToLayer("piso"))
            {
                impactado = true;
                Explotar();
                ControlBot scriptObjetoTocado = collision.gameObject.GetComponent<ControlBot>();
                if (scriptObjetoTocado != null)
                {
                    scriptObjetoTocado.recibirDaño(1000);
                }
            }
        }

        else if (collision.gameObject.layer == LayerMask.NameToLayer("enemigo"))
        {
            impactado = true;
            Explotar();
            ControlBot scriptObjetoTocado = collision.gameObject.GetComponent<ControlBot>();
            if (scriptObjetoTocado != null)
            {
                scriptObjetoTocado.recibirDaño(100);
            }
        }


    }

    private void Explotar()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radioExplosion);

        foreach (Collider nearbyObject in colliders)
        {
            if (nearbyObject.CompareTag("Bot"))
            {
                ControlBot scriptObjetoTocado = nearbyObject.GetComponent<ControlBot>();
                if (scriptObjetoTocado != null)
                {
                    scriptObjetoTocado.recibirDaño(100);
                }
            }

            else if (nearbyObject.CompareTag("Estructura"))
            {
                DañoEstructuras scriptObjetoTocado = nearbyObject.GetComponent<DañoEstructuras>();
                if (scriptObjetoTocado != null)
                {
                    scriptObjetoTocado.recibirDaño(1000);
                }
            }
        }

        if (!yaExplotado)
        {
            yaExplotado = true;
            if (explosionCohetePrefab != null)
            {
                GameObject particulas = Instantiate(explosionCohetePrefab, transform.position, Quaternion.identity) as GameObject;
                Destroy(particulas, 2);
            }

            GestorDeAudio.instancia.ReproducirSonido("Explosion");
            Destroy(gameObject);
        }
    }
}
