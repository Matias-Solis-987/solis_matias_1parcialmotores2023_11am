using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buff : MonoBehaviour
{
    public float aumentoDeRapidez = 2.0f;

    public void UsarItem(GameObject jugador)
    {
        if (jugador != null)
        {
            ControlJugador controlJugador = jugador.GetComponent<ControlJugador>();
            if (controlJugador != null)
            {
                controlJugador.rapidezDesplazamiento *= aumentoDeRapidez;
                StartCoroutine(RestablecerRapidezOriginal(controlJugador));
                GestorDeAudio.instancia.ReproducirSonido("FIAUM");
            }
        }
    }

    private IEnumerator RestablecerRapidezOriginal(ControlJugador controlJugador)
    {
        yield return new WaitForSeconds(5.0f);

        if (controlJugador != null)
        {
            controlJugador.rapidezDesplazamiento /= aumentoDeRapidez;
        }
    }
}
