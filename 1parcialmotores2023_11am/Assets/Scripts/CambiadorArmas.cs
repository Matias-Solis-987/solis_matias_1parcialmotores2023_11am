using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiadorArmas : MonoBehaviour
{
    public List<GameObject> armas = new List<GameObject>();
    public List<string> nombresDeArmas = new List<string> { "pistola", "sniper", "escopeta", "lanzagranadas", "lanzacohetes" }; //"lanzallamas"

    private int armaActual = 0;

    void Start()
    {
        armas = new List<GameObject>(transform.childCount);

        for (int i = 0; i < transform.childCount; i++)
        {
            armas.Add(transform.GetChild(i).gameObject);
        }

        if (armas.Count > 0 && armas.Count == nombresDeArmas.Count)
        {
            EquiparArma(0);
        }

        else
        {
            Debug.LogError("Error: Asegurate de tener la misma cantidad de armas y nombres de armas.");
        }
    }

    void Update()
    {
        for (int i = 1; i <= armas.Count; i++)
        {
            if (Input.GetKeyDown(KeyCode.Alpha0 + i))
            {
                EquiparArma(i - 1);
            }
        }
    }

    void EquiparArma(int indice)
    {
        foreach (GameObject arma in armas)
        {
            arma.SetActive(false);
        }

        armas[indice].SetActive(true);

        if (indice >= 0 && indice < nombresDeArmas.Count)
        {
            Debug.Log("Arma equipada: " + nombresDeArmas[indice]);
            armaActual = indice;
        }

        else
        {
            Debug.LogError("Error: Arma no valida.");
        }

        armaActual = indice;
    }

    public int ObtenerArmaActual()
    {
        return armaActual;
    }
}
