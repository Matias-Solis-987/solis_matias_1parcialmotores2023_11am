using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPistola : MonoBehaviour
{
    public GameObject proyectil;
    public Camera camaraPrimeraPersona;
    private bool enZoom = false;
    public float visionNormal = 60.0f;
    public float visionZoom = 30.0f;
    public float cadencia = 0.5f;
    private float tiempoUltimoDisparo = 0f;

    public void Disparar()
    {
        if (Input.GetMouseButtonDown(0) && Time.time > tiempoUltimoDisparo + cadencia)
        {
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            GameObject pro;
            pro = Instantiate(proyectil, ray.origin, transform.rotation);
            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraPrimeraPersona.transform.forward * 100, ForceMode.Impulse);
            RaycastHit hit;
            tiempoUltimoDisparo = Time.time;

            GestorDeAudio.instancia.ReproducirSonido("Disparo");
        }
    }

    public void Apuntar()
    {
        if (Input.GetMouseButtonDown(1))
        {
            enZoom = !enZoom;

            if (enZoom)
            {
                camaraPrimeraPersona.fieldOfView = visionZoom;
            }
            else
            {
                camaraPrimeraPersona.fieldOfView = visionNormal;
            }
        }
    }

    public void RestablecerZoom()
    {
        enZoom = false;
        camaraPrimeraPersona.fieldOfView = visionNormal;
    }
}
