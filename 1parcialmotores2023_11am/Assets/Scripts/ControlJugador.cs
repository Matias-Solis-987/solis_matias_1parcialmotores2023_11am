using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    public float Correr = 20.0f;
    public Camera camaraPrimeraPersona;

    public LayerMask capaPiso;
    public LayerMask capaPared;
    public LayerMask capaEnemigo;

    public SphereCollider col;
    private Rigidbody rb;

    public float magnitudSalto;
    private int saltosRealizados = 0;
    public int maxSaltos = 2;
    public int hp = 100;

    public float distanciaPiso = 100f;
    public float distanciaPared = 100f;
    public float distanciaEnemigo = 100f;

    public int cantTP = 2;
    public float tiempoEsperaTP = 2f;
    private bool enCooldown = false;

    public GameObject granada;
    public int cantGranadas = 1;

    public float tiempoDash = 0.5f;
    public float velocidadDash = 30.0f;
    private bool enDash = false;
    public float tiempoEsperaDash = 2f;

    public ControlPistola pistola;
    public ControlSniper sniper;
    public ControlEscopeta escopeta;
    public ControlLanzaGranadas lanzagranadas;
    public ControlLanzaCohetes lanzacohetes;
    //public ControlLanzaLlamas lanzallamas;
    private int ArmaActual = 0;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        col = GetComponent<SphereCollider>();

        GestorDeAudio.instancia.ReproducirSonido("Musica");
    }

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            movimientoAdelanteAtras *= Correr;
            movimientoCostados *= Correr;
        }

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyUp(KeyCode.LeftShift) || Input.GetKeyUp(KeyCode.RightShift))
        {
            movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento * Time.deltaTime;
            movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento * Time.deltaTime;
            transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
        }

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if (Input.GetKeyDown(KeyCode.Q) && cantGranadas > 0)
        {
            cantGranadas = Mathf.Max(0, cantGranadas - 1);
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            GameObject pro;
            pro = Instantiate(granada, ray.origin, transform.rotation);
            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraPrimeraPersona.transform.forward * 50, ForceMode.Impulse);
            Destroy(pro, 5);
            RaycastHit hit;

            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
            {
                Debug.Log("La granada toco al objeto: " + hit.collider.name);
            }

            if (hit.collider.name.Substring(0, 3) == "Bot")
            {
                GameObject objetoTocado = GameObject.Find(hit.transform.name);
                ControlBot scriptObjetoTocado = objetoTocado.GetComponent<ControlBot>();

                if (scriptObjetoTocado != null)
                {
                    scriptObjetoTocado.recibirDaņo(25);
                }
            }

            GestorDeAudio.instancia.ReproducirSonido("Granada");
            Debug.Log("Te quedan " + cantGranadas + " granadas de mano.");
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ArmaActual = 0;
            ControlArma(0);
            CambiarArma(0);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ArmaActual = 1;
            ControlArma(1);
            CambiarArma(1);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            ArmaActual = 2;
            ControlArma(2);
            CambiarArma(2);
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            ArmaActual = 3;
            ControlArma(3);
            CambiarArma(3);
        }

        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            ArmaActual = 4;
            ControlArma(4);
            CambiarArma(4);
        }

        //if (Input.GetKeyDown(KeyCode.Alpha6))
        //{
        //ArmaActual = 5;
        //ControlArma(5);
        //CambiarArma(5);
        //}

        if (Input.GetMouseButtonDown(0))
        {
            if (ArmaActual == 0)
            {
                pistola.Disparar();
            }

            else if (ArmaActual == 1)
            {
                sniper.Disparar();
            }

            else if (ArmaActual == 2)
            {
                escopeta.Disparar();
            }

            else if (ArmaActual == 3)
            {
                lanzagranadas.Disparar();
            }

            else if (ArmaActual == 4)
            {
                lanzacohetes.Disparar();
            }

            //else if (ArmaActual == 5)
            //{
            //lanzallamas.Disparar();
            //}
        }

        if (Input.GetMouseButtonDown(1))
        {
            if (ArmaActual == 0)
            {
                pistola.Apuntar();
            }

            else if (ArmaActual == 1)
            {
                sniper.Apuntar();
            }

            else if (ArmaActual == 2)
            {
                escopeta.Apuntar();
            }

            else if (ArmaActual == 3)
            {
                lanzagranadas.Apuntar();
            }

            else if (ArmaActual == 4)
            {
                lanzacohetes.Apuntar();
            }

            //else if (ArmaActual == 5)
            //{
            //lanzallamas.Apuntar();
            //}
        }

        if (EstaEnPiso() || EstaEnEnemigo())
        {
            saltosRealizados = 0;
        }

        if (Input.GetKeyDown(KeyCode.Space) && saltosRealizados < maxSaltos)
        {
            rb.velocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z);
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            saltosRealizados++;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            ReiniciarJuego();
        }

        if (Input.GetKeyDown(KeyCode.Tab) && EstaEnAire() && cantTP > 0 && !enCooldown)
        {
            cantTP = Mathf.Max(0, cantTP - 1);
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, distanciaPared, capaEnemigo))
            {
                if (hit.collider.CompareTag("Bot"))
                {
                    ControlBot scriptObjetoTocado = hit.collider.GetComponent<ControlBot>();
                    if (scriptObjetoTocado != null)
                    {
                        scriptObjetoTocado.recibirDaņo(1000);
                    }
                }

                StartCoroutine(TPCooldown());
                transform.position = hit.point + hit.normal * 0.5f;
                saltosRealizados = 0;
                GestorDeAudio.instancia.ReproducirSonido("TP");
                Debug.Log("Te quedan " + cantTP + " TPs.");
            }
        }

        if (Input.GetKeyDown(KeyCode.Tab) && EstaEnPiso() && cantTP > 0 && !enCooldown)
        {
            cantTP = Mathf.Max(0, cantTP - 1);
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, distanciaPared, capaEnemigo))
            {
                if (hit.collider.CompareTag("Bot"))
                {
                    ControlBot scriptObjetoTocado = hit.collider.GetComponent<ControlBot>();
                    if (scriptObjetoTocado != null)
                    {
                        scriptObjetoTocado.recibirDaņo(1000);
                    }
                }

                StartCoroutine(TPCooldown());
                transform.position = hit.point + hit.normal * 0.5f;
                saltosRealizados = 0;
                GestorDeAudio.instancia.ReproducirSonido("TP");
                Debug.Log("Te quedan " + cantTP + " TPs.");
            }
        }

        if (Input.GetKeyDown(KeyCode.C) && !enDash)
        {
            StartCoroutine(Dash());
            GestorDeAudio.instancia.ReproducirSonido("Dash");
        }
    } 
    void CambiarArma(int indiceArma)
    {
        if (indiceArma == 0)
        {
            ArmaActual = 0;
            pistola.enabled = true;
            sniper.enabled = false;
            escopeta.enabled = false;
            lanzagranadas.enabled = false;
            lanzacohetes.enabled = false;
            //lanzallamas.enabled = false;
        }

        else if (indiceArma == 1)
        {
            ArmaActual = 1;
            pistola.enabled = false;
            sniper.enabled = true;
            escopeta.enabled = false;
            lanzagranadas.enabled = false;
            lanzacohetes.enabled = false;
            //lanzallamas.enabled = false;
        }

        else if (indiceArma == 2)
        {
            ArmaActual = 2;
            pistola.enabled = false;
            sniper.enabled = false;
            escopeta.enabled = true;
            lanzagranadas.enabled = false;
            lanzacohetes.enabled = false;
            //lanzallamas.enabled = false;
        }

        else if (indiceArma == 3)
        {
            ArmaActual = 3;
            pistola.enabled = false;
            sniper.enabled = false;
            escopeta.enabled = false;
            lanzagranadas.enabled = true;
            lanzacohetes.enabled = false;
            //lanzallamas.enabled = false;
        }

        else if (indiceArma == 4)
        {
            ArmaActual = 4;
            pistola.enabled = false;
            sniper.enabled = false;
            escopeta.enabled = false;
            lanzagranadas.enabled = false;
            lanzacohetes.enabled = true;
            //lanzallamas.enabled = false;
        }

        //else if (indiceArma == 5)
        //{
            //ArmaActual = 5;
            //pistola.enabled = false;
            //sniper.enabled = false;
            //escopeta.enabled = false;
            //lanzagranadas.enabled = false;
            //lanzacohetes.enabled = false;
            //lanzallamas.enabled = true;
        //}
        RestablecerZoom();
    }

    void RestablecerZoom()
    {
        if (pistola != null)
        {
            pistola.RestablecerZoom();
        }

        if (sniper != null)
        {
            sniper.RestablecerZoom();
        }

        if (escopeta != null)
        {
            escopeta.RestablecerZoom();
        }

        if (lanzagranadas != null)
        {
            lanzagranadas.RestablecerZoom();
        }

        if (lanzacohetes != null)
        {
            lanzacohetes.RestablecerZoom();
        }

        //if (lanzallamas != null)
        //{
            //lanzallamas.RestablecerZoom();
        //}
    }

    void ControlArma(int indiceArma)
    {
        if (indiceArma == 0)
        {
            pistola.Disparar();
            pistola.Apuntar();
        }

        else if (indiceArma == 1)
        {
            sniper.Disparar();
            sniper.Apuntar();
        }

        else if (indiceArma == 2)
        {
            escopeta.Disparar();
            escopeta.Apuntar();
        }

        else if (indiceArma == 3)
        {
            lanzagranadas.Disparar();
            lanzagranadas.Apuntar();
        }

        else if (indiceArma == 4)
        {
            lanzacohetes.Disparar();
            lanzacohetes.Apuntar();
        }

        //else if (indiceArma == 5)
        //{
            //lanzallamas.Disparar();
            //lanzallamas.Apuntar();
        //}

        else
        {
            Debug.LogError("Indice de arma fuera de rango.");
        }
    }

    public void ReiniciarJuego()
    {
        Scene escenaActual = SceneManager.GetActiveScene();
        SceneManager.LoadScene("SampleScene");
    }

    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * 0.9f, capaPiso);
    }

    private bool EstaEnEnemigo()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * 0.9f, capaEnemigo);
    }

    private bool EstaEnAire()
    {
        return !EstaEnPiso();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable"))
        {
            if (other.name == "Buff")
            {
                Buff buff = other.GetComponent<Buff>();
                buff.UsarItem(gameObject);
            }
            else if (other.name == "Victoria")
            {
                SceneManager.LoadScene("Ganaste");
            }
            other.gameObject.SetActive(false);
        }
    }

    IEnumerator Dash()
    {
        enDash = true;
        Vector3 velocidadAntesDash = rb.velocity;
        rb.velocity = transform.forward * velocidadDash;
        yield return new WaitForSeconds(tiempoDash);
        yield return new WaitForSeconds(tiempoEsperaDash);
        rb.velocity = velocidadAntesDash;
        enDash = false;
    }

    IEnumerator TPCooldown()
    {
        enCooldown = true;

        yield return new WaitForSeconds(tiempoEsperaTP);

        enCooldown = false;
    }

    public void recibirDaņoJ(int daņo)
    {
        hp = hp - daņo;
        Debug.Log("HP actual: " + hp);

        if (hp <= 0)
        {
            Debug.Log("Te mataron");
            ExplosionEnemigo script = GetComponent<ExplosionEnemigo>();
            if (script != null)
            {
                script.Explosion();
                GestorDeAudio.instancia.ReproducirSonido("Muerte");
            }
            SceneManager.LoadScene("Menu muerte");
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("proyectil"))
        {
            recibirDaņoJ(1);
        }
    }
}
