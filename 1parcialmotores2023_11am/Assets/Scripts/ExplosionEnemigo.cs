using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionEnemigo : MonoBehaviour
{
    public GameObject explosion;

    public void Explotar(int segundos)
    {
        Invoke("Explosion", segundos);
        Destroy(gameObject, segundos + 2);
    }

    public void Explosion()
    {
        GameObject particulas = Instantiate(explosion, transform.position, Quaternion.identity) as GameObject;
        Destroy(particulas, 2);
    }
}
