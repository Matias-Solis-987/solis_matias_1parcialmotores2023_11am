using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJuego : MonoBehaviour
{
    public GameObject jugador;
    public GameObject bot;
    private List<GameObject> listaEnemigos = new List<GameObject>();
    float tiempoRestante;
    private bool juegoIniciado = false;

    void Start()
    {
        StartCoroutine(ComenzarCronometro(60));
    }

    void Update()
    {
        if (juegoIniciado && tiempoRestante <= 0)
        {
            juegoIniciado = false;
            ComenzarJuego();
        }

        if (jugador.transform.position.y < -30f)
        {
            ComenzarJuego();
        }

        if (!juegoIniciado)
        {
            juegoIniciado = true;
            jugador.transform.position = new Vector3(0f, 0f, 0f);
        }
    }

    void ComenzarJuego()
    {
        Debug.Log("ComenzarJuego() llamado.");

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        Debug.Log("ComenzarJuego() finalizado.");

        jugador.transform.position = new Vector3(0f, 0f, 0f);

        if (listaEnemigos.Count > 0)
        {
            foreach (GameObject item in listaEnemigos)
            {
                Destroy(item);
            }
            listaEnemigos.Clear();
        }

        listaEnemigos.Add(Instantiate(bot, new Vector3(5, 1f, 3f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(bot, new Vector3(3, 1f, 3f), Quaternion.identity));
        listaEnemigos.Add(Instantiate(bot, new Vector3(1, 1f, 3f), Quaternion.identity));

        StartCoroutine(ComenzarCronometro(60));

        Debug.Log("ComenzarJuego() finalizado.");
    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 60)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante > 0)
        {
            Debug.Log("Restan " + tiempoRestante + " segundos.");
            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
        }
    }

    public void EscenaJuego()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void MenuJuego()
    {
        SceneManager.LoadScene("Menu de inicio");
    }

    public void MenuControles()
    {
        SceneManager.LoadScene("Controles");
    }

    public void MenuArmas()
    {
        SceneManager.LoadScene("Menu armas");
    }

    public void MenuPistola()
    {
        SceneManager.LoadScene("Menu pistola");
    }

    public void MenuSniper()
    {
        SceneManager.LoadScene("Menu Sniper");
    }

    public void MenuEscopeta()
    {
        SceneManager.LoadScene("Menu escopeta");
    }

    public void MenuLanzaGranadas()
    {
        SceneManager.LoadScene("Menu lanza granadas");
    }

    public void MenuLanzaCohetes()
    {
        SceneManager.LoadScene("Menu lanza cohetes");
    }

    public void MenuGanador()
    {
        SceneManager.LoadScene("Menu ganador");
    }

    public void SalirDelJuego()
    {
        Application.Quit();
    }
}
