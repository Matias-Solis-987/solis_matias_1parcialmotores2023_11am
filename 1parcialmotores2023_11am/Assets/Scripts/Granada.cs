using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Granada : MonoBehaviour
{
    public float retardo = 3f;
    public float radioExplosion = 10f;
    public GameObject explosionGranadaPrefab;
    public bool yaExplotada = false;

    void Start()

    {
        StartCoroutine(ExplotarConRetardo(retardo));
    }

    IEnumerator ExplotarConRetardo(float delay)
    {
        yield return new WaitForSeconds(delay);

        Collider[] colliders = Physics.OverlapSphere(transform.position, radioExplosion);

        foreach (Collider nearbyObject in colliders)
        {
            if (nearbyObject.CompareTag("Bot"))
            {
                ControlBot scriptObjetoTocado = nearbyObject.GetComponent<ControlBot>();
                if (scriptObjetoTocado != null)
                {
                    scriptObjetoTocado.recibirDaño(100);
                }
            }

            else if (nearbyObject.CompareTag("Estructura"))
            {
                DañoEstructuras scriptObjetoTocado = nearbyObject.GetComponent<DañoEstructuras>();
                if (scriptObjetoTocado != null)
                {
                    scriptObjetoTocado.recibirDaño(1000);
                }
            }
        }

        if (!yaExplotada)
        {
            yaExplotada = true;
            if (explosionGranadaPrefab != null)
            {
                GameObject particulas = Instantiate(explosionGranadaPrefab, transform.position, Quaternion.identity) as GameObject;
                Destroy(particulas, 2);
            }

            GestorDeAudio.instancia.ReproducirSonido("Explosion");
            Destroy(gameObject);
        }
    }
}
