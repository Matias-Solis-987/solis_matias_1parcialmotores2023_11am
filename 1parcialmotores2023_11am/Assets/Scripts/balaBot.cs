using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class balaBot : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.layer == LayerMask.NameToLayer("jugador"))
        {
            if (this.CompareTag("proyectil") && CompareTag("Player"))
            {
                ControlJugador scriptObjetoTocado = collision.gameObject.GetComponent<ControlJugador>();
                if (scriptObjetoTocado != null)
                {
                    scriptObjetoTocado.recibirDaņoJ(1);
                    Destroy(this.gameObject);
                }
            }
        }
    }
}
