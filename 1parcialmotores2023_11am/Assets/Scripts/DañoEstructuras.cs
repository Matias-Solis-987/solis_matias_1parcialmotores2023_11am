using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DañoEstructuras : MonoBehaviour
{

    public int hp = 1000;
    public GameObject explosionPrefab;

    public void recibirDaño(int daño)
    {
        hp = hp - daño;
        Debug.Log("HP actual: " + hp);

        if (hp <= 0)
        {
            Debug.Log("Estructura destruida");
            ExplosionEnemigo script = GetComponent<ExplosionEnemigo>();
            if (script != null)
            {
                script.Explosion();
            }
            GestorDeAudio.instancia.ReproducirSonido("DestruccionEstructura");
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        switch (collision.gameObject.tag)
        {
            case "bala":
                recibirDaño(25);
                break;

            case "balaSniper":
                recibirDaño(25);
                break;

            case "balaEscopeta":
                recibirDaño(25);
                break;

            case "Granada":
                recibirDaño(0);
                break;

            case "cohete":
                recibirDaño(1000);
                break;

            default:
                break;
        }
    }

    private void desaparecer()
    {
        Explotar();
        Destroy(gameObject);
    }

    private void Explotar()
    {
        if (explosionPrefab != null)
        {
            GameObject particulas = Instantiate(explosionPrefab, transform.position, Quaternion.identity) as GameObject;
            Destroy(particulas, 2);
        }
    }
}
