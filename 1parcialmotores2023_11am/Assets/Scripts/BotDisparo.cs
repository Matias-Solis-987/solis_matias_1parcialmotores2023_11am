using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotDisparo : MonoBehaviour
{
    public GameObject proyectil;
    public Transform puntoDisparo;
    public float frecuenciaDisparo = 2.0f;
    public float fuerzaDisparo = 10.0f;
    public float rangoDeVision = 10.0f;

    private bool puedeDisparar = true;

    void Update()
    {
        GameObject jugador = BuscarJugadorEnRango();

        if (jugador != null && puedeDisparar)
        {
            Disparar(jugador.transform);
            StartCoroutine(Recargar());
        }
    }

    IEnumerator Recargar()
    {
        puedeDisparar = false;
        yield return new WaitForSeconds(1.0f);
        puedeDisparar = true;
    }

    GameObject BuscarJugadorEnRango()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, rangoDeVision);

        foreach (Collider collider in hitColliders)
        {
            if (collider.CompareTag("jugador"))
            {
                return collider.gameObject;
            }
        }

        return null;
    }

    void Disparar(Transform objetivo)
    {
        Vector3 direccion = (objetivo.position - puntoDisparo.position).normalized;

        Quaternion rotacion = Quaternion.LookRotation(direccion);

        GameObject proyectilInstance = Instantiate(proyectil, puntoDisparo.position, rotacion);
        Rigidbody proyectilRB = proyectilInstance.GetComponent<Rigidbody>();
        proyectilInstance.tag = "proyectil";

        proyectilRB.velocity = direccion * fuerzaDisparo;

        GestorDeAudio.instancia.ReproducirSonido("Disparo");
    }
}
