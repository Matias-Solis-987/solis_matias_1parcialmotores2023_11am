using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlSniper : MonoBehaviour
{
    public int cantBalas = 5;
    public GameObject proyectilSniper;
    public Camera camaraPrimeraPersona;
    private bool enZoom = false;
    public float visionNormal = 60.0f;
    public float visionZoom = 10.0f;
    public float cadencia = 2f;
    private float tiempoUltimoDisparo = 0f;

    public void Disparar()
    {
        if (Input.GetMouseButtonDown(0) && cantBalas > 0 && Time.time > tiempoUltimoDisparo + cadencia)
        {
            cantBalas = Mathf.Max(0, cantBalas - 1);
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            GameObject pro;
            pro = Instantiate(proyectilSniper, ray.origin, transform.rotation);
            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraPrimeraPersona.transform.forward * 150, ForceMode.Impulse);
            RaycastHit hit;
            tiempoUltimoDisparo = Time.time;

            GestorDeAudio.instancia.ReproducirSonido("DisparoSniper");
        }
    }

    public void Apuntar()
    {
        if (Input.GetMouseButtonDown(1))
        {
            enZoom = !enZoom;

            if (enZoom)
            {
                camaraPrimeraPersona.fieldOfView = visionZoom;
            }
            else
            {
                camaraPrimeraPersona.fieldOfView = visionNormal;
            }
        }
    }

    public void RestablecerZoom()
    {
        enZoom = false;
        camaraPrimeraPersona.fieldOfView = visionNormal;
    }
}
