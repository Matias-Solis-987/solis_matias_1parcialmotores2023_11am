using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaltoBot : MonoBehaviour 
{ 
    bool tengoQueBajar = false;
    public int salto = 10; 
    void Update() 
    { 
        if (transform.position.y >= 8) 
        { 
            tengoQueBajar = true;
        } 
        if (transform.position.y <= 2) 
        { tengoQueBajar = false; 
        } 
        if (tengoQueBajar) 
        { 
            Bajar();
        } 
        else 
        { 
            Saltar(); 
        } 
    }
    void Saltar()
    { 
        transform.position += transform.up * salto * Time.deltaTime; 
    } 
    void Bajar()
    { 
        transform.position -= transform.up * salto * Time.deltaTime;
    } 
}
