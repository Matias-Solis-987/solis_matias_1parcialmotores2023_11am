using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaSniper : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.layer == LayerMask.NameToLayer("enemigo"))
        {
            if (this.CompareTag("balaSniper") && CompareTag("Sniper"))
            {
                ControlBot scriptObjetoTocado = collision.gameObject.GetComponent<ControlBot>();
                if (scriptObjetoTocado != null)
                {
                    scriptObjetoTocado.recibirDaņo(100);
                    Destroy(this.gameObject);
                }
            }
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("Pared") ||
            collision.gameObject.layer == LayerMask.NameToLayer("piso"))
        {
            if (this.CompareTag("balaSniper") && CompareTag("Sniper"))
            {
                ControlBot scriptObjetoTocado = collision.gameObject.GetComponent<ControlBot>();
                if (scriptObjetoTocado != null)
                {
                    scriptObjetoTocado.recibirDaņo(25);
                    Destroy(this.gameObject);
                }
            }
        }
    }
}
