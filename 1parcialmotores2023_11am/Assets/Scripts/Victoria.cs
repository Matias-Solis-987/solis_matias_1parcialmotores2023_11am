using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Victoria : MonoBehaviour
{
    private bool jugadorGano = false;
    public TextMeshProUGUI mensajeVictoria;
    using GameManager gameManager;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("jugador") && !jugadorGano)
        {
            jugadorGano = true;
            mensajeVictoria.text = "Ganaste";
            mensajeVictoria.enabled = true;
            GestorDeAudio.instancia.ReproducirSonido("Ganador");
            GameManager gameManager = FindObjectOfType<GameManager>();
            if (gameManager != null)
            {
                gameManager.CargarMenuGanador();
            }
        }
    }
}
