using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlLanzaCohetes : MonoBehaviour
{
    public int cantCohetes = 5;
    public GameObject Cohete;
    public Camera camaraPrimeraPersona;
    private bool enZoom = false;
    public float visionNormal = 60.0f;
    public float visionZoom = 30.0f;
    public float cadencia = 0.5f;
    private float tiempoUltimoDisparo = 0f;

    public void Disparar()
    {
        if (Input.GetMouseButtonDown(0) && cantCohetes > 0 && Time.time > tiempoUltimoDisparo + cadencia)
        {
            cantCohetes = Mathf.Max(0, cantCohetes - 1);
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            GameObject pro;
            pro = Instantiate(Cohete, ray.origin, transform.rotation);
            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraPrimeraPersona.transform.forward * 100, ForceMode.Impulse);
            Destroy(pro, 5);
            RaycastHit hit;
            tiempoUltimoDisparo = Time.time;

            GestorDeAudio.instancia.ReproducirSonido("DisparoLanzaCohetes");
            Debug.Log("Te quedan " + cantCohetes + " cohetes.");
        }
    }

    public void Apuntar()
    {
        if (Input.GetMouseButtonDown(1))
        {
            enZoom = !enZoom;

            if (enZoom)
            {
                camaraPrimeraPersona.fieldOfView = visionZoom;
            }
            else
            {
                camaraPrimeraPersona.fieldOfView = visionNormal;
            }
        }
    }

    public void RestablecerZoom()
    {
        enZoom = false;
        camaraPrimeraPersona.fieldOfView = visionNormal;
    }
}
